# Firebase Emulators

| Service           |         Address         |
| :---------------- | :---------------------: |
| Authentication    | `http://localhost:9099` |
| Realtime Database | `http://localhost:9000` |
| Storage           | `http://localhost:9199` |